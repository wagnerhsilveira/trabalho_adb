const rethinkdb = require('rethinkdb');
let connection = null;
const config = {
    host: 'ec2-54-164-241-156.compute-1.amazonaws.com',
    port: 28015
}
const onConnect = (err, conn) => {
    if (err) throw err
    connection = conn
    console.log('Connected to rethink db')
}
rethinkdb.connect(config , onConnect)


/**
 * 
 * Insert a new user in the database
 * 
 * @param {*} {id: user-generatedId} 
 */
const insertUser = (data) => {
    return rethinkdb.db('chat')
        .table('users')
        .insert(data)
        .run(connection, (err, result) => {
            return new Promise((resolve, reject) => {
                if(err) {
                    reject(err)
                }
                resolve(result)
            })
        });
}

const insertMessage = (message) => {
    const data = message
    data.timestamp = Date.now()
    return rethinkdb.db('chat')
        .table('messages')
        .insert(data)
        .run(connection, (err, result) => {
            return new Promise((resolve, reject) => {
                if(err) {
                    reject(err)
                }
                resolve(result)
            })
        });
}
/**
 * 
 * Recover an user by its ID
 * 
 * @param {*} {id: user-generatedId} 
 */
const getUserById = (id) => {
    return rethinkdb.db('chat')
        .table('users')
        .get(id)
        .run(connection, (err, result) => {
            return new Promise((resolve, reject) => {
                if(err) {
                    reject(err)
                }
                resolve(result)
            })
        });
}
/**
 * 
 * Recover all users in a room
 * 
 * @param {*} {id: user-generatedId} 
 */
const getUsersByRoom = (room) => {
    return rethinkdb.db('chat')
        .table('users')
        .filter({room})
        .run(connection, (err, result) => {
            return new Promise((resolve, reject) => {
                if(err) {
                    reject(err)
                }
                resolve(result)
            })
        });
}

module.exports = {
    insertUser,
    insertMessage,
    getUserById,
    onConnect
}