const io = require('socket.io')(8081);
const {
    insertUser, 
    getUserById,
    insertMessage
} = require('./db')

io.on('connection', function (socket) {
    


  socket.on('send-message', async function (user) {

    const message = {
        from: user.name,
        message: user.message,
        room: user.room
    }
    try{
        await insertMessage(message)
        io.to(user.room).emit('message', message)
    }catch(e){
        console.log(e)
    }

  });

  
  socket.on('login', async function(data){
    try {
     let newUser = await insertUser(data)
     const insertedUser = await getUserById(newUser.generated_keys[0])
     socket.join(insertedUser.room);
     io.to(insertedUser.room).emit('userJoined', insertedUser)
    } catch(e){
        io.emit('error', e)
    }
  })

  socket.on('disconnect', function () {
    io.emit('user disconnected');
  });
});
      