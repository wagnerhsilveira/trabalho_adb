import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { onCommand, command } from '../services/websocket'


class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      name: '',
      room: '',
      isLoggedIn: false
    }
  }
  componentDidMount = () =>{
    
    onCommand('userJoined', e => {
        console.log("joined", e)
        sessionStorage.setItem('user', e)
        this.setState({isLoggedIn:true, user: e})
    })
  }
  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }
  doLogin = (e) => {
    e.preventDefault()
    command('login', this.state)
  }
  render() {
    if(this.state.isLoggedIn){
        return <Redirect to={{
            pathname:  `/room/${this.state.user.room}`,
            state: { user: this.state.user }
          }}/>
    }
    return (
      <div>
        <header>
          <h1>Mini-chat</h1>
        </header>
        <section>
          <form>
            <fieldset>
              <label htmlFor='name'> Qual seu nome? </label>
              <input name='name' 
                    type='text' 
                    onChange={this.onChange}
                    value={this.state.name}/>
              <br/>
              <label htmlFor='room'> Qual a sala </label>
              <input name='room' 
                      type='text' 
                      value={this.state.room} 
                      onChange={this.onChange}/>
              <br/>
              <button onClick={this.doLogin}> Entrar </button>
            </fieldset>
          </form>
        </section>
      </div>
    );
  }
}

export default Login;
