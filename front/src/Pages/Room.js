import React, { Component } from 'react';
import { onCommand, command } from '../services/websocket'
import moment from 'moment'
import './room.css'

class Room extends Component {
  constructor(props){
    super(props)
    this.state = {
      message: '',
      messages: [],
      user: this.props.location.state.user
    }
  }
  componentDidMount = () =>{
   
    onCommand('message', e => {
      const element = document.createElement('p')
      const date = moment(e.timestamp).format("DD-MM-YYYY HH:mm:ss")
      element.innerHTML=`<p>[${date}] <strong> ${e.from}: </strong>  ${e.message}</p>`
      document.getElementById('messages').appendChild(element)
    })
  }
  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }
  sendMessage = (e) => {
    e.preventDefault()
    let user = this.state.user;
    user.message = this.state.message;
    command('send-message', user)
    this.setState({message: ''})
  }
  onWrite = (e) => {
      this.setState({[e.target.name]: e.target.value})
  }
  render() {
    const props = this.props.location.state
    return (
      <div className='main'>
        <header>
           <h1>Olá {props.user.name}</h1>
           <h2>Você está na sala: {props.user.room}</h2>
        </header>
        <section id='messages'></section>
        <section className='inputs'>
            <input type='text' name='message' value={this.state.message} onChange={this.onWrite}/>
            <input type='button' value={'enviar'} onClick={this.sendMessage}/>
        </section>
      </div>
    );
  }
}

export default Room;
