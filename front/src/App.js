import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import React, { Component } from 'react'
import Login from './Pages/Login'
import Room from './Pages/Room'


class App extends Component {
  render() {
    return <Router>
            <Switch>
              <Route exact component={ Login } path='/' />
              <Route exact component={ Room }  path='/room/:room' />
            </Switch>
          </Router>
     
  }
}

export default App;
