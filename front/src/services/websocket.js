import openSocket from 'socket.io-client'
const socket = openSocket('http://ec2-54-164-241-156.compute-1.amazonaws.com:8081/')

 
export function onCommand(command, cb) {
    socket.on(command, message => cb(message));
}

export function command(command, data) {
    socket.emit(command, data);
}